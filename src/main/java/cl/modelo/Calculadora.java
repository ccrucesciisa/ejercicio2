/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.modelo;




/**
 *
 * @author ccruces
 */
public class Calculadora {
    private int valor1;
    private int valor2;
    private int resultadoSuma;
    
    

    /**
     * @return the valor1
     */
    public int getValor1() {
        
        return valor1;
    }

    /**
     * @param valor1 the valor1 to set
     */
    public void setValor1(int valor1) {
        this.valor1 = valor1;
    }

    /**
     * @return the valor2
     */
    public int getValor2() {
        return valor2;
    }

    /**
     * @param valor2 the valor2 to set
     */
    public void setValor2(int valor2) {
        this.valor2 = valor2;
    }

    /**
     * @return the resultadoSuma
     */
    public int getResultadoSuma() {
        
        int suma=this.valor1+this.valor2;
        return suma;
    }


    
    /**
     * @param resultadoSuma the resultadoSuma to set
     */
    public void setResultadoSuma(int resultadoSuma) {
        this.resultadoSuma = resultadoSuma;
    }
    

    
}
