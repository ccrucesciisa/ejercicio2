<%-- 
    Document   : index
    Created on : 01-04-2021, 19:20:07
    Author     : ccruces
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="CalculadoraControlador" method="GET">

            <label for="valor1">Ingrese Valor 1:</label>
            <input type="text" id="valor1" name="valor1">

            <label for="valor2">Ingrese Valor 2:</label>
            <input type="text" id="valor2" name="valor2">

            <button type="submit" class="btn btn-success">ir al servicio</button>

        </form>
    </body>
</html>
